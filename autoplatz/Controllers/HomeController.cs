﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using autoplatz.Models;
using autoplatz.Data;
using autoplatz.Services;

namespace autoplatz.Controllers
{
    public class HomeController : Controller
    {

        private readonly ApplicationDbContext _context;

        private readonly CarService carService;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;

            carService = new CarService(_context);

        }

        public IActionResult Index(List<Car> cars)
        {
            if (!cars.Any())
            {
                return View(_context.Car.Where(c=>c.IsAvailable==true).ToList());
            }
            return View(cars.Where(c=>c.IsAvailable==true).ToList());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        //[HttpGet, ActionName("Index")]
        //public IActionResult FilterByAge(int year)
        //{
        //    List<Car> cars = carService.FilterCarsByAge(year).ToList();
        //    return View(cars);
        //}

        //[HttpPost, ActionName("Index")]
        //public IActionResult FilterByModel(string Model)
        //{
        //    List<Car> cars = carService.FilterCarsByModel(Model).ToList();
        //    return View(cars);
        //}

        [HttpPost, ActionName("Index")]
        public IActionResult FilterByAllAtributes([Bind("Color,Model,Engine,HorsePower,Price,Mileage,Year")] Car car, double? PriceTo, int? HorsePowerTo, double? MileageTo, int? YearTo) {
            List<Car> cars = carService.FilterByAll(car, PriceTo, HorsePowerTo, MileageTo, YearTo);
            object Search = new Search(){
                Color = car.Color,
                Engine = car.Engine,
                Model = car.Model,
                HorsePower = car.HorsePower,
                Price = car.Price,
                Mileage = car.Mileage, 
                Year = car.Year,
                PriceTo = PriceTo,
                HorsePowerTo = HorsePowerTo,
                MileageTo= MileageTo,
                YearTo = YearTo
            };

            ViewData["search"] = Search as Search;
            return View(cars);
        }




    }

    public class Search {
        public string Color { get; set; }
        public string Engine { get; set; }
        public string Model { get; set; }
        public int? HorsePower { get; set; }
        public double? Price { get; set; }
        public double? Mileage { get; set; }
        public int? Year { get; set; }
        public double? PriceTo { get; set; }
        public int? HorsePowerTo{ get; set; }
        public double? MileageTo { get; set; }
        public int? YearTo { get; set; }

    }

}
