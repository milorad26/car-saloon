USE [master]
GO
/****** Object:  Database [autoplatz]    Script Date: 4/23/2019 5:42:05 PM ******/
CREATE DATABASE [autoplatz]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'autoplatz', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\autoplatz.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'autoplatz_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\autoplatz_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [autoplatz] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [autoplatz].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [autoplatz] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [autoplatz] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [autoplatz] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [autoplatz] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [autoplatz] SET ARITHABORT OFF 
GO
ALTER DATABASE [autoplatz] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [autoplatz] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [autoplatz] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [autoplatz] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [autoplatz] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [autoplatz] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [autoplatz] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [autoplatz] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [autoplatz] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [autoplatz] SET  DISABLE_BROKER 
GO
ALTER DATABASE [autoplatz] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [autoplatz] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [autoplatz] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [autoplatz] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [autoplatz] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [autoplatz] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [autoplatz] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [autoplatz] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [autoplatz] SET  MULTI_USER 
GO
ALTER DATABASE [autoplatz] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [autoplatz] SET DB_CHAINING OFF 
GO
ALTER DATABASE [autoplatz] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [autoplatz] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [autoplatz] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [autoplatz] SET QUERY_STORE = OFF
GO
USE [autoplatz]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 4/23/2019 5:42:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[Street] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[CountryID] [int] NOT NULL,
	[ApplicationUserID] [nvarchar](450) NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[DOB] [datetime2](7) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Car]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car](
	[CarID] [int] IDENTITY(1,1) NOT NULL,
	[Color] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Engine] [nvarchar](max) NULL,
	[HorsePower] [int] NULL,
	[Price] [float] NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[IsAvailable] [bit] NOT NULL,
	[Mileage] [float] NULL,
	[Year] [int] NULL,
 CONSTRAINT [PK_Car] PRIMARY KEY CLUSTERED 
(
	[CarID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[PaymentMethod] [nvarchar](max) NULL,
	[IsPaid] [bit] NOT NULL,
	[ApplicationUserID] [nvarchar](450) NULL,
	[CarID] [int] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TestDrive]    Script Date: 4/23/2019 5:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestDrive](
	[TestDriveID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[IsApproved] [bit] NOT NULL,
	[CarID] [int] NOT NULL,
	[ApplicationUserID] [nvarchar](450) NULL,
 CONSTRAINT [PK_TestDrive] PRIMARY KEY CLUSTERED 
(
	[TestDriveID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'00000000000000_CreateIdentitySchema', N'2.2.4-servicing-10062')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190416150750_InitialCreate', N'2.2.4-servicing-10062')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190416151555_AdditionalCreate', N'2.2.4-servicing-10062')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190418104029_testdrive', N'2.2.4-servicing-10062')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190419081422_addedAddresses', N'2.2.4-servicing-10062')
SET IDENTITY_INSERT [dbo].[Address] ON 

INSERT [dbo].[Address] ([AddressID], [Street], [City], [CountryID], [ApplicationUserID]) VALUES (1, N'Alekse Nenadovica 3', N'Subotica', 216, N'453f64c6-97de-4674-880f-aed07a826272')
INSERT [dbo].[Address] ([AddressID], [Street], [City], [CountryID], [ApplicationUserID]) VALUES (4, N'Alekse Nenadovica 3', N'Beograd', 191, N'40186630-1bce-4bbb-b8bb-4faba5bb9b8e')
INSERT [dbo].[Address] ([AddressID], [Street], [City], [CountryID], [ApplicationUserID]) VALUES (5, N'Jovana Rajica 5', N'Beograd', 5, N'3d961265-f2bb-47a6-8388-080c988d115d')
SET IDENTITY_INSERT [dbo].[Address] OFF
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'29bc79ab-9fa7-4e30-84b2-2279fad699fe', N'Admin', N'ADMIN', N'7bec23bb-731b-4d11-b6f9-57602881dab0')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'd556da8d-50f1-4ca6-be8b-dd678a377717', N'Customer', N'CUSTOMER', N'90a95b36-dcd0-4134-84c7-30df3e9d5c26')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'6badc9f5-eb2b-4b19-8c90-0a8131c291ba', N'29bc79ab-9fa7-4e30-84b2-2279fad699fe')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3d961265-f2bb-47a6-8388-080c988d115d', N'd556da8d-50f1-4ca6-be8b-dd678a377717')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'40186630-1bce-4bbb-b8bb-4faba5bb9b8e', N'd556da8d-50f1-4ca6-be8b-dd678a377717')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'453f64c6-97de-4674-880f-aed07a826272', N'd556da8d-50f1-4ca6-be8b-dd678a377717')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9f48ab0e-6b9c-4eef-811a-98f55e46224b', N'd556da8d-50f1-4ca6-be8b-dd678a377717')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ae3fa211-7e73-4609-9518-8de8a26e1daa', N'd556da8d-50f1-4ca6-be8b-dd678a377717')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [DOB], [FirstName], [LastName]) VALUES (N'3d961265-f2bb-47a6-8388-080c988d115d', N'stefan', N'STEFAN', N'stefan@stefan', N'STEFAN@STEFAN', 0, N'AQAAAAEAACcQAAAAEFeprRj3uP5qjuRsXMuLoGbeHTUB9zDyDX4kq52duSrk+H+70mNWz+oejNQfbPa2Vg==', N'KUTEXVMGW7U2WTFOXBOEYNV4VSPXHI74', N'194cafb5-bcfa-4bd8-989c-256014f6e81c', N'+381 69 555 888', 0, 0, NULL, 1, 0, CAST(N'1998-01-17T00:00:00.0000000' AS DateTime2), N'Stefan', N'Atanaskovic')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [DOB], [FirstName], [LastName]) VALUES (N'40186630-1bce-4bbb-b8bb-4faba5bb9b8e', N'zeljko', N'ZELJKO', N'zeljkojelicic@hotmail.com', N'ZELJKOJELICIC@HOTMAIL.COM', 0, N'AQAAAAEAACcQAAAAEPml7ESXzLZRyKZIwQJU9tQEhvRxhvN82x2NjThgWFjyNAlHNToC3SPzo7DIqy4V8A==', N'ATBWY33DTQGDNBVGSN3ETGJOJK244TEM', N'658ae860-eb3e-4b4c-97e7-3287daf96b78', N'+381 69 555 888', 0, 0, NULL, 1, 0, CAST(N'2019-04-17T00:00:00.0000000' AS DateTime2), N'Zeljko', N'Jelicic')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [DOB], [FirstName], [LastName]) VALUES (N'453f64c6-97de-4674-880f-aed07a826272', N'Nenad', N'NENAD', N'nenad@hotmail.com', N'NENAD@HOTMAIL.COM', 0, N'AQAAAAEAACcQAAAAEBXWYgpsfE6w6ik1FmZDkpsWui7PhRU7refIYLjbYaxxSBrUvGCGRqeF6o3JLvlLFA==', N'OZ43PRBB3LTHMNECQZC5OOOTMCMUIUW6', N'6f6e74ab-955a-442b-a923-9681ffa4ebdb', N'+381 69 555 888', 0, 0, NULL, 1, 0, CAST(N'2019-04-17T00:00:00.0000000' AS DateTime2), N'Nenad', N'Krantic')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [DOB], [FirstName], [LastName]) VALUES (N'6badc9f5-eb2b-4b19-8c90-0a8131c291ba', N'admin@admin.com', N'ADMIN@ADMIN.COM', N'admin@admin.com', N'ADMIN@ADMIN.COM', 0, N'AQAAAAEAACcQAAAAEAxHdJpsKna63O764v4/X4NnRHl2Jwij7p1mNh0MJG+oZdJcEOHLgGQdRiZbau6T6g==', N'OKHKEYDVQCTTXQ57S3KBZUKBCDA3XUWB', N'45354c75-254a-4d5b-b555-d5ca7f0aa8f8', NULL, 0, 0, NULL, 1, 0, CAST(N'2019-04-17T11:41:38.3247681' AS DateTime2), NULL, NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [DOB], [FirstName], [LastName]) VALUES (N'9f48ab0e-6b9c-4eef-811a-98f55e46224b', N'milorad', N'MILORAD', N'milorad@milorad', N'MILORAD@MILORAD', 0, N'AQAAAAEAACcQAAAAEFSvo9wNaPTo5e+EcGbni5KDiMTYYRz2Uz7OMyC3aFsvGXyGExKK4MmHmGA2ayfZPQ==', N'IDRAFTYWRJGZAATUNNX3525XWZWIJD52', N'005f78b0-cf82-4c93-a9fe-a95a55b29aa7', NULL, 0, 0, NULL, 1, 0, CAST(N'2019-04-17T11:41:38.3247681' AS DateTime2), NULL, NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [DOB], [FirstName], [LastName]) VALUES (N'ae3fa211-7e73-4609-9518-8de8a26e1daa', N'Borisa', N'BORISA', N'borisa@borisa.com', N'BORISA@BORISA.COM', 0, N'AQAAAAEAACcQAAAAELmosdNK/edv/zblfDDwHqpnaG4u9f82gjpW1cX5ATQ+qzcPyamgeff39Gt2FXUVVQ==', N'DICUOFBPS2ND36ELIYW7AYVEZ5E3KRTR', N'd9c4db2f-8cb6-47a8-a116-71d1e32a7811', NULL, 0, 0, NULL, 1, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Car] ON 

INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (1, N'Black', N'Skoda Octavia', N'1.9 TDI', 175, 3550, N'http://bay2car.com/img/2005-Skoda-Octavia-1-9-TDI-PD-Elegance-5dr-331675753500/0.jpg', 1, 175000, 2008)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (2, N'White', N'BMW', N'2.2 CDI', 190, 12255, N'https://www.bmwinocala.com/assets/stock/expanded/white/640/2019bms20_640/2019bms200001_640_07.jpg?', 1, 95000, 2010)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (3, N'Silver', N'Audi A3', N'TFSI 1.4', 125, 12500, N'https://images3.polovniautomobili.com/user-images/thumbs/1416/14166703/236c37a0fd23-1920x1080.jpg', 0, 135000, 2011)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (4, N'Red', N'Chevrolet Aveo', N' 1.4 B', 101, 4500, N'https://images3.polovniautomobili.com/user-images/thumbs/1428/14280343/07f4979332b2-1920x1080.jpg', 1, 145000, 2011)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (6, N'Blue', N'Ford Focus', N'Gasoline', 120, 5000, N'https://autovolostorage.blob.core.windows.net/advertimages-5210205/-focus-2017-721l.jpg', 1, 20000, 2010)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (7, N'Red', N'BMW X5', N'1.8 TDI', 200, 30000, N'https://cdn.bmwblog.com/wp-content/uploads/2018/03/Melbourne-Red-BMW-X5-M-HRE-Wheels-Image-2.jpg', 1, 0, 2016)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (8, N'Gray', N'Renault Megan ', N'1.9 Gasoline', 110, 7000, N'https://img5.lalafo.com/i/posters/original/d0/c7/c292dfb5d31264cf092605f25a07.jpeg', 0, 85000, 2009)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (9, N'Blue', N'Mitsubishi Lancer', N'1.6 Gasoline', 180, 12000, N'https://www.tradecarview.com/cdn/trade/img06/cars/2455216/21736338/japan%20car/2011+mitsubishi+lancer/01w.jpg?height=525&width=700&type=resize', 1, 20000, 2011)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (10, N'Gray', N'Opel Corsa', N'1.3 CDTI', 69, 2000, N'https://images3.polovniautomobili.com/user-images/thumbs/1396/13966956/7f0fa17d77b2-1920x1080.jpg', 1, 95000, 2004)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (11, N'Black', N'Toyota Avensis', N'Diesel ', 116, 4300, N'https://images3.polovniautomobili.com/user-images/thumbs/1419/14190558/6865b3f158d1-1920x1080.jpg', 1, 128000, 2004)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (12, N'Orange', N'Dacia Duster Essential', N'1.5 dCi 4x4', 109, 16500, N'https://images3.polovniautomobili.com/user-images/thumbs/1425/14255359/e11b160fc9e7-1920x1080.jpg', 1, 7700, 2018)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (13, N'Red', N'Fiat 500L', N'Gasoline 1.3', 95, 13000, N'https://images3.polovniautomobili.com/user-images/thumbs/1409/14091633/c7a831b8d216-1920x1080.jpg', 1, 0, 2019)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (14, N'Black', N'Mercedes s350 4matic', N'Diesel  3.0', 258, 75000, N'https://images3.polovniautomobili.com/user-images/thumbs/1158/11580057/519caf36c096-1920x1080.jpg', 1, 90000, 2016)
INSERT [dbo].[Car] ([CarID], [Color], [Model], [Engine], [HorsePower], [Price], [ImageUrl], [IsAvailable], [Mileage], [Year]) VALUES (15, N'White', N'Audi A6', N'Diesel 1.9', 190, 25000, N'https://images3.polovniautomobili.com/user-images/thumbs/1322/13220122/c140faf82bf1-1920x1080.jpg', 1, 174000, 2014)
SET IDENTITY_INSERT [dbo].[Car] OFF
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (1, N'Andorra')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (2, N'Afghanistan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (3, N'Antigua and Barbuda')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (4, N'Anguilla')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (5, N'Albania')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (6, N'Armenia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (7, N'Angola')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (8, N'Antarctica')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (9, N'Argentina')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (10, N'American Samoa')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (11, N'Australia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (12, N'Aruba')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (13, N'Azerbaijan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (14, N'Austria')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (15, N'Algeria')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (16, N'Bahamas')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (17, N'Bahrain')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (18, N'Bangladesh')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (19, N'Barbados')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (20, N'Belarus')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (21, N'Belgium')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (22, N'Belize')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (23, N'Benin')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (24, N'Bermuda')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (25, N'Bhutan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (26, N'Plurinational State of Bolivia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (27, N'Sint Eustatius and Saba Bonaire')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (28, N'Bosnia and Herzegowina')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (29, N'Botswana')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (30, N'Bouvet Island')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (31, N'Brazil')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (32, N'British Indian Ocean Territory')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (33, N'Brunei Darussalam')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (34, N'Bulgaria')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (35, N'Burkina Faso')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (36, N'Burundi')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (37, N'Cambodia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (38, N'Cameroon')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (39, N'Canada')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (40, N'Cape Verde')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (41, N'Cayman Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (42, N'Central African Republic')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (43, N'Chad')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (44, N'Chile')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (45, N'China')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (46, N'Christmas Island')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (47, N'Cocos  Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (48, N'Colombia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (49, N'Comoros')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (50, N'Congo')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (51, N'The Democratic Republic of The Congo')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (52, N'Cook Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (53, N'Costa Rica')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (54, N'Croatia ')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (55, N'Cuba')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (56, N'Cyprus')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (57, N'Czech Republic')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (58, N'Denmark')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (59, N'Djibouti')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (60, N'Dominica')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (61, N'Dominican Republic')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (62, N'Ecuador')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (63, N'Egypt')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (64, N'El Salvador')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (65, N'Equatorial Guinea')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (66, N'Eritrea')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (67, N'Estonia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (68, N'Ethiopia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (69, N'Falkland Islands ')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (70, N'Faroe Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (71, N'Fiji')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (72, N'Finland')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (73, N'France')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (74, N'French Guiana')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (75, N'French Polynesia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (76, N'French Southern Territories')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (77, N'Gabon')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (78, N'Gambia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (79, N'Georgia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (80, N'Germany')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (81, N'Ghana')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (82, N'Gibraltar')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (83, N'Greece')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (84, N'Greenland')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (85, N'Grenada')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (86, N'Guadeloupe')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (87, N'Guam')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (88, N'Guatemala')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (89, N'Guernsey')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (90, N'Guinea')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (91, N'Guinea-bissau')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (92, N'Guyana')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (93, N'Haiti')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (94, N'Heard and McDonald Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (95, N'Holy See ')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (96, N'Honduras')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (97, N'Hong Kong')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (98, N'Hungary')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (99, N'Iceland')
GO
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (100, N'India')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (101, N'Indonesia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (102, N'Iran ')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (103, N'Iraq')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (104, N'Ireland')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (105, N'Isle of Man')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (106, N'Israel')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (107, N'Italy')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (108, N'Jamaica')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (109, N'Japan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (110, N'Jersey')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (111, N'Jordan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (112, N'Kazakhstan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (113, N'Kenya')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (114, N'Kiribati')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (115, N'Democratic People''s Republic of Korea')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (116, N'Republic of Korea')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (117, N'Kuwait')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (118, N'Kyrgyzstan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (119, N'Lao People''s Democratic Republic')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (120, N'Latvia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (121, N'Lebanon')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (122, N'Lesotho')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (123, N'Liberia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (124, N'Libya')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (125, N'Liechtenstein')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (126, N'Lithuania')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (127, N'Luxembourg')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (128, N'Macao')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (129, N'The Former Yugoslav Republic of Macedonia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (130, N'Madagascar')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (131, N'Malawi')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (132, N'Malaysia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (133, N'Maldives')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (134, N'Mali')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (135, N'Malta')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (136, N'Marshall Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (137, N'Martinique')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (138, N'Mauritania')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (139, N'Mauritius')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (140, N'Mayotte')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (141, N'Mexico')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (142, N'Federated States of Micronesia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (143, N'Republic of Moldova')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (144, N'Monaco')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (145, N'Mongolia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (146, N'Montenegro')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (147, N'Montserrat')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (148, N'Morocco')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (149, N'Mozambique')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (150, N'Myanmar')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (151, N'Namibia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (152, N'Nauru')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (153, N'Nepal')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (154, N'Netherlands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (155, N'New Caledonia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (156, N'New Zealand')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (157, N'Nicaragua')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (158, N'Niger')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (159, N'Nigeria')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (160, N'Niue')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (161, N'Norfolk Island')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (162, N'Northern Mariana Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (163, N'Norway')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (164, N'Oman')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (165, N'Pakistan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (166, N'Palau')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (167, N'State of Palestine')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (168, N'Panama')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (169, N'Papua New Guinea')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (170, N'Paraguay')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (171, N'Peru')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (172, N'Philippines')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (173, N'Pitcairn')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (174, N'Poland')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (175, N'Portugal')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (176, N'Puerto Rico')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (177, N'Qatar')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (178, N'Romania')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (179, N'Russian Federation')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (180, N'Rwanda')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (181, N'Ascension and Tristan Da Cunha Saint Helena')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (182, N'Saint Kitts and Nevis')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (183, N'Saint Lucia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (184, N'Saint Pierre and Miquelon')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (185, N'Saint Vincent and The Grenadines')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (186, N'Samoa')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (187, N'San Marino')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (188, N'Sao Tome and Principe')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (189, N'Saudi Arabia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (190, N'Senegal')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (191, N'Serbia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (192, N'Seychelles')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (193, N'Sierra Leone')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (194, N'Singapore')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (195, N'Sint Maarten ')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (196, N'Slovakia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (197, N'Slovenia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (198, N'Solomon Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (199, N'Somalia')
GO
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (200, N'South Africa')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (201, N'South Georgia and The South Sandwich Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (202, N'South Sudan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (203, N'Spain')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (204, N'Sri Lanka')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (205, N'Sudan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (206, N'Suriname')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (207, N'Svalbard and Jan Mayen Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (208, N'Swaziland')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (209, N'Sweden')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (210, N'Switzerland')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (211, N'Syrian Arab Republic')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (212, N'Province of China Taiwan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (213, N'Tajikistan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (214, N'United Republic of Tanzania')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (215, N'Thailand')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (216, N'Timor-leste')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (217, N'Togo')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (218, N'Tokelau')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (219, N'Tonga')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (220, N'Trinidad and Tobago')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (221, N'Tunisia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (222, N'Turkey')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (223, N'Turkmenistan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (224, N'Turks and Caicos Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (225, N'Tuvalu')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (226, N'Uganda')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (227, N'Ukraine')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (228, N'United Arab Emirates')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (229, N'United Kingdom')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (230, N'United States')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (231, N'United States Minor Outlying Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (232, N'Uruguay')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (233, N'Uzbekistan')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (234, N'Vanuatu')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (235, N'Bolivarian Republic of Venezuela')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (236, N'Vietnam')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (237, N'Virgin Islands (British)')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (238, N'Virgin Islands (US)')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (239, N'Wallis and Futuna Islands')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (240, N'Western Sahara')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (241, N'Yemen')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (242, N'Zambia')
INSERT [dbo].[Country] ([CountryID], [Name]) VALUES (243, N'Zimbabwe')
SET IDENTITY_INSERT [dbo].[Country] OFF
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([OrderID], [Date], [PaymentMethod], [IsPaid], [ApplicationUserID], [CarID]) VALUES (13, CAST(N'2019-04-19T16:51:27.8400000' AS DateTime2), N'Cash €', 1, N'453f64c6-97de-4674-880f-aed07a826272', 3)
INSERT [dbo].[Order] ([OrderID], [Date], [PaymentMethod], [IsPaid], [ApplicationUserID], [CarID]) VALUES (1002, CAST(N'2019-04-23T15:20:45.1830000' AS DateTime2), N'Cash €', 0, N'453f64c6-97de-4674-880f-aed07a826272', 8)
SET IDENTITY_INSERT [dbo].[Order] OFF
SET IDENTITY_INSERT [dbo].[TestDrive] ON 

INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (4, CAST(N'2019-04-20T10:29:17.0000000' AS DateTime2), 1, 1, N'453f64c6-97de-4674-880f-aed07a826272')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (7, CAST(N'2019-04-18T16:24:07.0000000' AS DateTime2), 1, 1, N'453f64c6-97de-4674-880f-aed07a826272')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (8, CAST(N'2019-04-18T16:24:35.0000000' AS DateTime2), 0, 2, N'40186630-1bce-4bbb-b8bb-4faba5bb9b8e')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (9, CAST(N'2019-04-18T17:25:33.0000000' AS DateTime2), 0, 1, N'40186630-1bce-4bbb-b8bb-4faba5bb9b8e')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (10, CAST(N'2019-04-19T17:48:16.0000000' AS DateTime2), 0, 1, N'453f64c6-97de-4674-880f-aed07a826272')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (11, CAST(N'2019-04-19T17:58:31.0000000' AS DateTime2), 0, 1, N'453f64c6-97de-4674-880f-aed07a826272')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (14, CAST(N'2019-04-19T18:07:00.0000000' AS DateTime2), 0, 1, N'453f64c6-97de-4674-880f-aed07a826272')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (16, CAST(N'2019-04-19T18:09:58.0000000' AS DateTime2), 0, 1, N'453f64c6-97de-4674-880f-aed07a826272')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (17, CAST(N'2019-04-19T18:51:46.0000000' AS DateTime2), 0, 1, N'453f64c6-97de-4674-880f-aed07a826272')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (18, CAST(N'2019-04-19T18:53:59.0000000' AS DateTime2), 0, 6, N'453f64c6-97de-4674-880f-aed07a826272')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (19, CAST(N'2019-04-20T23:00:33.0000000' AS DateTime2), 0, 2, N'ae3fa211-7e73-4609-9518-8de8a26e1daa')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (20, CAST(N'2019-04-19T19:03:40.0000000' AS DateTime2), 0, 7, N'ae3fa211-7e73-4609-9518-8de8a26e1daa')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (21, CAST(N'2019-04-19T19:03:57.0000000' AS DateTime2), 0, 10, N'ae3fa211-7e73-4609-9518-8de8a26e1daa')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (22, CAST(N'2019-04-19T19:04:04.0000000' AS DateTime2), 0, 13, N'ae3fa211-7e73-4609-9518-8de8a26e1daa')
INSERT [dbo].[TestDrive] ([TestDriveID], [Date], [IsApproved], [CarID], [ApplicationUserID]) VALUES (23, CAST(N'2019-04-19T19:51:41.0000000' AS DateTime2), 0, 9, N'3d961265-f2bb-47a6-8388-080c988d115d')
SET IDENTITY_INSERT [dbo].[TestDrive] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Address_ApplicationUserID]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Address_ApplicationUserID] ON [dbo].[Address]
(
	[ApplicationUserID] ASC
)
WHERE ([ApplicationUserID] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Address_CountryID]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE NONCLUSTERED INDEX [IX_Address_CountryID] ON [dbo].[Address]
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Order_ApplicationUserID]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE NONCLUSTERED INDEX [IX_Order_ApplicationUserID] ON [dbo].[Order]
(
	[ApplicationUserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Order_CarID]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Order_CarID] ON [dbo].[Order]
(
	[CarID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TestDrive_ApplicationUserID]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE NONCLUSTERED INDEX [IX_TestDrive_ApplicationUserID] ON [dbo].[TestDrive]
(
	[ApplicationUserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TestDrive_CarID]    Script Date: 4/23/2019 5:42:08 PM ******/
CREATE NONCLUSTERED INDEX [IX_TestDrive_CarID] ON [dbo].[TestDrive]
(
	[CarID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  CONSTRAINT [DF__AspNetUsers__DOB__4AB81AF0]  DEFAULT ('1900-01-01T00:00:00.0000000') FOR [DOB]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_AspNetUsers_ApplicationUserID] FOREIGN KEY([ApplicationUserID])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_AspNetUsers_ApplicationUserID]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Country_CountryID] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Country] ([CountryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Country_CountryID]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_AspNetUsers_ApplicationUserID] FOREIGN KEY([ApplicationUserID])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_AspNetUsers_ApplicationUserID]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Car_CarID] FOREIGN KEY([CarID])
REFERENCES [dbo].[Car] ([CarID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Car_CarID]
GO
ALTER TABLE [dbo].[TestDrive]  WITH CHECK ADD  CONSTRAINT [FK_TestDrive_AspNetUsers_ApplicationUserID] FOREIGN KEY([ApplicationUserID])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[TestDrive] CHECK CONSTRAINT [FK_TestDrive_AspNetUsers_ApplicationUserID]
GO
ALTER TABLE [dbo].[TestDrive]  WITH CHECK ADD  CONSTRAINT [FK_TestDrive_Car_CarID] FOREIGN KEY([CarID])
REFERENCES [dbo].[Car] ([CarID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TestDrive] CHECK CONSTRAINT [FK_TestDrive_Car_CarID]
GO
USE [master]
GO
ALTER DATABASE [autoplatz] SET  READ_WRITE 
GO
