﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autoplatz.Models
{
    public class TestDrive
    {
        public int TestDriveID { get; set; }
        public DateTime Date { get; set; }
        public bool IsApproved { get; set; }
        public int CarID { get; set; }
        public string ApplicationUserID{ get; set; }

        public Car Car { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

    }
}
