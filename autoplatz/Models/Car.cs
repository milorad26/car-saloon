﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autoplatz.Models
{
    public class Car
    {

        public int CarID { get; set; }
        public string Color { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }
        public int? HorsePower { get; set; }
        public double? Price { get; set; }
        public string ImageUrl { get; set; }
        public bool IsAvailable { get; set; }
        public double? Mileage { get; set; }
        public int? Year { get; set; }

        public Order Order { get; set; }
        public List<TestDrive> TestDrives { get; set; }

    }
}
