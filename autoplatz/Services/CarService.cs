﻿using autoplatz.Data;
using autoplatz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autoplatz.Services
{
    public class CarService
    {

        public ApplicationDbContext applicationDbContext;

        public CarService(ApplicationDbContext applicationDbContext) {
            this.applicationDbContext = applicationDbContext;
        }

        public List<Car> FilterCarsByAge(int Age) {
            var cars = applicationDbContext.Car.Where(c=>c.Year>Age).ToList();
            return cars;
        }

        public List<Car> FilterCarsByModel(string Model) {
            var cars = applicationDbContext.Car.Where(c => c.Model.Contains(Model)).ToList();
            return cars;
        }

        public List<Car> FilterByAll(Car car, double? PriceTo, int? HorsePowerTo, double? MileageTo, int? YearTo) {
            List<Car> cars = applicationDbContext.Car.ToList();
            if (car.Color!=null) {
                cars = cars.Where(c=>c.Color.ToLower().Contains(car.Color.ToLower())).ToList();
            }
            if (car.Model != null)
            {
                cars = cars.Where(c => c.Model.ToLower().Contains(car.Model.ToLower())).ToList();
            }
            if (car.Engine != null)
            {
                cars = cars.Where(c => c.Engine.ToLower().Contains(car.Engine.ToLower())).ToList();
            }
            if (car.HorsePower != null || HorsePowerTo != null)
            {
                if (car.HorsePower != null && HorsePowerTo != null)
                {
                    cars = cars.Where(c => c.HorsePower >= car.HorsePower && c.HorsePower <= HorsePowerTo).ToList();
                }
                else if (car.HorsePower == null && HorsePowerTo != null)
                {
                    cars = cars.Where(c => c.HorsePower <= HorsePowerTo).ToList();
                }
                else
                {
                    cars = cars.Where(c => c.HorsePower >= car.HorsePower).ToList();
                }
            }
            if (car.Price != null || PriceTo !=null)
            {
                if (car.Price != null && PriceTo != null)
                {
                    cars = cars.Where(c => c.Price >= car.Price && c.Price <= PriceTo).ToList();
                }
                else if (car.Price == null && PriceTo != null)
                {
                    cars = cars.Where(c => c.Price <= PriceTo).ToList();
                }
                else
                {
                    cars = cars.Where(c => c.Price >= car.Price).ToList();
                }
            }
            if (car.Mileage != null || MileageTo != null)
            {
                if (car.Mileage != null && MileageTo != null)
                {
                    cars = cars.Where(c => c.Mileage >= car.Mileage && c.Mileage <= MileageTo).ToList();
                }
                else if (car.Mileage == null && MileageTo != null)
                {
                    cars = cars.Where(c => c.Mileage <= MileageTo).ToList();
                }
                else
                {
                    cars = cars.Where(c => c.Mileage >= car.Mileage).ToList();
                }
            }
            if (car.Year != null || YearTo != null)
            {
                if (car.Year != null && YearTo != null)
                {
                    cars = cars.Where(c => c.Year >= car.Year && c.Year <= YearTo).ToList();
                }
                else if (car.Year == null && YearTo != null)
                {
                    cars = cars.Where(c => c.Year <= YearTo).ToList();
                }
                else
                {
                    cars = cars.Where(c => c.Year >= car.Year).ToList();
                }
            }

            return cars;
        }

    }
}
