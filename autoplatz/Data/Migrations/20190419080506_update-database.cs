﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace autoplatz.Data.Migrations
{
    public partial class updatedatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Address_ApplicationUserID",
                table: "Address");

            migrationBuilder.AlterColumn<int>(
                name: "Year",
                table: "Car",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<double>(
                name: "Mileage",
                table: "Car",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.CreateIndex(
                name: "IX_Address_ApplicationUserID",
                table: "Address",
                column: "ApplicationUserID",
                unique: true,
                filter: "[ApplicationUserID] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Address_ApplicationUserID",
                table: "Address");

            migrationBuilder.AlterColumn<int>(
                name: "Year",
                table: "Car",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Mileage",
                table: "Car",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Address_ApplicationUserID",
                table: "Address",
                column: "ApplicationUserID");
        }
    }
}
