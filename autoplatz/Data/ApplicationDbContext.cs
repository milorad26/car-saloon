﻿using System;
using System.Collections.Generic;
using System.Text;
using autoplatz.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace autoplatz.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<autoplatz.Models.Address> Address { get; set; }
        public DbSet<autoplatz.Models.Car> Car { get; set; }
        public DbSet<autoplatz.Models.Country> Country { get; set; }
        public DbSet<autoplatz.Models.Order> Order { get; set; }
        public DbSet<autoplatz.Models.TestDrive> TestDrive { get; set; }
    }
}
