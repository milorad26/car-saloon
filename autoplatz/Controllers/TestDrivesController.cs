﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using autoplatz.Data;
using autoplatz.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace autoplatz.Controllers
{
    public class TestDrivesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;

        public TestDrivesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        // GET: TestDrives
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.TestDrive.Include(t => t.ApplicationUser).Include(t => t.Car);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: TestDrives/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testDrive = await _context.TestDrive
                .Include(t => t.ApplicationUser)
                .Include(t => t.Car)
                .FirstOrDefaultAsync(m => m.TestDriveID == id);
            if (testDrive == null)
            {
                return NotFound();
            }

            return View(testDrive);
        }

        // GET: TestDrives/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["ApplicationUserID"] = new SelectList(_context.Users, "Id", "Id");
            ViewData["CarID"] = new SelectList(_context.Car, "CarID", "CarID");
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Customer")]
        public IActionResult Reserve(int id) {
            
                DateTime date = DateTime.Now.Date;
                Car Car = _context.Car.Find(id);
                ApplicationUser ApplicationUser = _context.Users.Find(userManager.GetUserId(HttpContext.User));
                TestDrive testDrive = new TestDrive
                {
                    CarID = id,
                    ApplicationUserID = userManager.GetUserId(HttpContext.User),
                    Car = Car,
                    ApplicationUser = ApplicationUser,
                    Date = DateTime.Parse(DateTime.Now.AddHours(2).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss"))

                };
                return View(testDrive);
           

        }

        // POST: TestDrives/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("TestDriveID,Date,IsApproved,CarID,ApplicationUserID")] TestDrive testDrive)
        {
            if (ModelState.IsValid)
            {
                _context.Add(testDrive);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ApplicationUserID"] = new SelectList(_context.Users, "Id", "Id", testDrive.ApplicationUserID);
            ViewData["CarID"] = new SelectList(_context.Car, "CarID", "CarID", testDrive.CarID);
            return View(testDrive);
        }
        
        [HttpPost]
        [Authorize(Roles = "Admin, Customer")]
        public IActionResult SaveTestDrive([Bind("TestDriveID,Date,IsApproved,CarID,ApplicationUserID")] TestDrive testDrive) {
            _context.Add(testDrive);
            _context.SaveChanges();
            return RedirectToAction("MyTestDrives");
        }



        // GET: TestDrives/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testDrive = await _context.TestDrive.FindAsync(id);
            if (testDrive == null)
            {
                return NotFound();
            }
            ViewData["ApplicationUserID"] = new SelectList(_context.Users, "Id", "Id", testDrive.ApplicationUserID);
            ViewData["CarID"] = new SelectList(_context.Car, "CarID", "CarID", testDrive.CarID);
            return View(testDrive);
        }

        // POST: TestDrives/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("TestDriveID,Date,IsApproved,CarID,ApplicationUserID")] TestDrive testDrive)
        {
            if (id != testDrive.TestDriveID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(testDrive);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TestDriveExists(testDrive.TestDriveID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ApplicationUserID"] = new SelectList(_context.Users, "Id", "Id", testDrive.ApplicationUserID);
            ViewData["CarID"] = new SelectList(_context.Car, "CarID", "CarID", testDrive.CarID);
            return View(testDrive);
        }

        // GET: TestDrives/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var testDrive = await _context.TestDrive
                .Include(t => t.ApplicationUser)
                .Include(t => t.Car)
                .FirstOrDefaultAsync(m => m.TestDriveID == id);
            if (testDrive == null)
            {
                return NotFound();
            }

            return View(testDrive);
        }

        // POST: TestDrives/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var testDrive = await _context.TestDrive.FindAsync(id);
            _context.TestDrive.Remove(testDrive);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TestDriveExists(int id)
        {
            return _context.TestDrive.Any(e => e.TestDriveID == id);
        }

        [Authorize(Roles = "Admin, Customer")]
        public IActionResult MyTestDrives() {
            ApplicationUser ApplicationUser = _context.Users.Find(userManager.GetUserId(HttpContext.User));
            List<TestDrive> approvedTestDrives = _context.TestDrive.Include(t=>t.Car).Where(t => t.ApplicationUserID == ApplicationUser.Id && t.IsApproved==true).ToList();
            List<TestDrive> unapprovedTestDrives = _context.TestDrive.Include(t => t.Car).Where(t => t.ApplicationUserID == ApplicationUser.Id && t.IsApproved==false).ToList();
            ViewData["approvedTestDrives"] = approvedTestDrives as List<TestDrive>;
            ViewData["unapprovedTestDrives"] = unapprovedTestDrives as List<TestDrive>;
            return View();
        }

        [Authorize(Roles = "Admin,Customer")]
        public IActionResult MyTestDriveDetails(int id) {
            TestDrive testDrive = _context.TestDrive.Include(t=>t.Car).Include(t => t.ApplicationUser).Where(t=>t.TestDriveID== id).FirstOrDefault();
            return View(testDrive);

        }
    }

}
