﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autoplatz.Models
{
    public class Order
    {
        public int OrderID { get; set; }
        public DateTime Date { get; set; }
        public string PaymentMethod { get; set; }
        public bool IsPaid { get; set; }
        public string ApplicationUserID { get; set; }
        public int CarID { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
        public Car Car { get; set; }



    }
}
