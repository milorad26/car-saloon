﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using autoplatz.Data;
using autoplatz.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Authorization;

namespace autoplatz.Controllers
{
    public class OrdersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        

        public OrdersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        //


        // GET: Orders
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Order.Include(o => o.ApplicationUser).Include(o => o.Car);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Orders/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order
                .Include(o => o.ApplicationUser)
                .Include(o => o.Car)
                .FirstOrDefaultAsync(m => m.OrderID == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Orders/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["ApplicationUserID"] = new SelectList(_context.Users, "Id", "Id");
            ViewData["CarID"] = new SelectList(_context.Car, "CarID", "CarID");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderID,Date,PaymentMethod,IsPaid,ApplicationUserID,CarID")] Order order)
        {
            if (ModelState.IsValid)
            {
                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ApplicationUserID"] = new SelectList(_context.Users, "Id", "Id", order.ApplicationUserID);
            ViewData["CarID"] = new SelectList(_context.Car, "CarID", "CarID", order.CarID);
            return View(order);
        }


        // GET: Orders/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            ViewData["ApplicationUserID"] = new SelectList(_context.Users, "Id", "Id", order.ApplicationUserID);
            ViewData["CarID"] = new SelectList(_context.Car, "CarID", "CarID", order.CarID);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("OrderID,Date,PaymentMethod,IsPaid,ApplicationUserID,CarID")] Order order)
        {
            if (id != order.OrderID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.OrderID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ApplicationUserID"] = new SelectList(_context.Users, "Id", "Id", order.ApplicationUserID);
            ViewData["CarID"] = new SelectList(_context.Car, "CarID", "CarID", order.CarID);
            return View(order);
        }

        // GET: Orders/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order
                .Include(o => o.ApplicationUser)
                .Include(o => o.Car)
                .FirstOrDefaultAsync(m => m.OrderID == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Order.FindAsync(id);
            _context.Order.Remove(order);
            Car car = _context.Car.Find(order.CarID);
            car.IsAvailable = true;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Order.Any(e => e.OrderID == id);
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Customer")]
        public IActionResult SaveOrder([Bind("OrderID,Date,PaymentMethod,IsPaid,ApplicationUserID,CarID")] Order order) {

            Car car = _context.Car.Find(order.CarID);
            car.IsAvailable = false;
            _context.Car.Update(car);
            _context.Order.Add(order);
            _context.SaveChanges();
            return RedirectToAction("MyOrders");

        }

        [Authorize(Roles = "Admin, Customer")]
        [HttpGet, ActionName("OrderCar")]
        public IActionResult OrderCar(int id) {
            Car Car = _context.Car.Find(id);
           
                ApplicationUser ApplicationUser = _context.Users.Find(userManager.GetUserId(HttpContext.User));

                Order order = new Order
                {
                    Date = DateTime.Now,
                    CarID = id,
                    Car = Car,
                    ApplicationUser = ApplicationUser,
                    ApplicationUserID = ApplicationUser.Id
                };

                return View(order);

        }

        [Authorize(Roles = "Admin, Customer")]
        public IActionResult MyOrders() {
            ApplicationUser ApplicationUser = _context.Users.Find(userManager.GetUserId(HttpContext.User));
            List<Order> paidOrders = _context.Order.Include(o=>o.Car).Where(o=>o.ApplicationUserID==ApplicationUser.Id && o.IsPaid==true).ToList();
            List<Order> unpaidOrders = _context.Order.Include(o => o.Car).Where(o => o.ApplicationUserID == ApplicationUser.Id && o.IsPaid == false).ToList();
            ViewData["PaidOrders"] = paidOrders as List<Order>;
            ViewData["UnpaidOrders"] = unpaidOrders as List<Order>;
            return View();
        }

        [Authorize(Roles = "Admin, Customer")]
        public IActionResult MyOrderDetails(int id) {
            Order order = _context.Order.Include(u=>u.ApplicationUser.Address.Country).Include(u=>u.Car).FirstOrDefault(o=>o.OrderID==id);
            return View(order);

        }
    }
}
