﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autoplatz.Models
{
    public class Address
    {
        public int AddressID { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public int CountryID { get; set; }
        public string ApplicationUserID { get; set; }

        public Country Country { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

    }
}
