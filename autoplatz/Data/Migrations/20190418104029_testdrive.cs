﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace autoplatz.Data.Migrations
{
    public partial class testdrive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Price",
                table: "Car",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "HorsePower",
                table: "Car",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Price",
                table: "Car",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HorsePower",
                table: "Car",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
