﻿using autoplatz.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autoplatz.Services
{
    public class OrderService
    {

        public ApplicationDbContext applicationDbContext;

        public OrderService(ApplicationDbContext applicationDbContext) {
            this.applicationDbContext = applicationDbContext;
        }

        public void CreateOffer(OrderService order) {
            applicationDbContext.Add(order);
        }

    }
}
